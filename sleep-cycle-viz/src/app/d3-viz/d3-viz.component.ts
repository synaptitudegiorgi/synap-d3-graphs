import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import * as d3 from 'd3';
import { AxisDomain } from 'd3';
import { getDAttr } from '../helpers';
import { Datum } from '../data/models'

@Component({
  selector: 'app-d3-viz',
  templateUrl: './d3-viz.component.html',
  styleUrls: ['./d3-viz.component.css']
})
export class D3VizComponent implements AfterViewInit, OnInit {
  @ViewChild('sleepviz') container;
  @Input('data') data;
  xScale: d3.AxisScale<AxisDomain>;
  yScale: d3.AxisScale<AxisDomain>;
  width: number;
  height = 450;
  margin = {
    top: 0,
    right: 15,
    bottom: 50,
    left: 50
  };
  levels = {
    wake: 4,
    rem: 3,
    light: 2,
    deep: 1 
  };

  // curve thickness
  curveInnerHeight = 10;

  chart: d3.Selection<d3.BaseType, {}, HTMLElement, any>;

  constructor() { }

  ngOnInit() {
  }
  
  ngAfterContentChecked() {
    if (this.data) {
      this.setScales();
      this.drawAxis();
      this.main();
      this.drawShortData();
    }
  }


  ngAfterViewInit() {
    const bclient = this.container.nativeElement.getBoundingClientRect();
    this.width = bclient.width - 10;
  }

  /**
   * sets x and y axis
   */
  setScales() {
    const xDomain = [this.data.levels.data[0].dateTime, this.data.endTime];
    const yDomain = ["WAKE", "REM", "LIGHT", "DEEP"];

    // time scale for x axis
    this.xScale = d3.scaleTime()
                  .domain([new Date(xDomain[0]), new Date(xDomain[1])])
                  .range([0, this.width - this.margin.left - this.margin.right]);

    // band scale for y axis
    this.yScale = d3.scaleBand()
                .domain(yDomain)
                .range([0, this.height - this.margin.top - this.margin.bottom]);
  }

  /**
   * creates vertical y and horizontal x axis based on scales
   */
  getAxis() {
    const xAxis = d3.axisBottom(this.xScale);
    const yAxis = d3.axisLeft(this.yScale);
    return {
      xAxis: xAxis,
      yAxis: yAxis
    };
  }

  /**
   * draws x and y axes
   */
  drawAxis() {
    const axes = this.getAxis();
    this.chart = d3.select("#svg-container")
                  .append("g")
                  .attr("transform", `translate(${this.margin.left}, ${this.margin.top})`);

    this.chart.append("g")
        .attr("class", "xAxis")
         .attr("transform", `translate(0, ${this.height - this.margin.top - this.margin.bottom})`)
         .call(axes.xAxis);
    
    this.chart.append("g")
        .attr("class", "yAxis")
         .call(axes.yAxis);
  }

  /**
   * main drawing function
   */
  main() {
    // have a sleep data as a single variable
    let sleepData = this.data.levels.data;

    // append g container for path elements
    let paths = this.chart.append("g").attr("class", "path-elements");

    // iterate over sleep data and draw curves and vertical lines
    sleepData.forEach((d,i) => {
      let curveHeight = 40, direction;
      
      // calculate curve direction
      if (i == 0) {
        direction = this.detectDirection(d, null, sleepData[i + 1]);
      }
      else if (i == sleepData.length - 1) {
        direction = this.detectDirection(d, sleepData[i - 1], null);
      }
      else {
        direction = this.detectDirection(d, sleepData[i-1], sleepData[i+1]);
      }
      
      // calculate coordinates
      let startCoords = {
        x: this.xScale(new Date(d.dateTime)),
        y: this.yScale(d.level.toUpperCase()) + this.yScale.bandwidth() / 2 + this.curveInnerHeight / 2
      }
      
      // adjust curve height
      if (direction == 'up' || direction == 'down') {
        curveHeight -= 16;
      }

      const curveWidth = this.getCurveWidth(d)

      // draw path elements
      paths.append("path")
          .attr("d", getDAttr(startCoords, curveWidth, curveHeight, this.curveInnerHeight, direction))
          .attr("fill", "url(#MyGradient)")
          .attr("stroke", "url(#MyGradient)")
          .attr("stroke-width", 2);
      
      // draw vertical lines
      if (i > 0) {
        var M = [
                  startCoords.x, 
                  this.yScale(sleepData[i - 1].level.toUpperCase()) + this.yScale.bandwidth() / 2 + this.curveInnerHeight / 2
                ];

        var L = [startCoords.x, startCoords.y];
        
        // fix spikes and line overflows
        if (direction == 'curlyR' || direction == 'up') {
          if (i == 1) {
            M[1] -= curveHeight
          } else {
            M[1] -= curveHeight - 16;
          }
        }

        if (direction == 'roundRDown') {
          M[1] -= curveHeight - 16
        }

        // draw path elements
        paths.append("path")
            .attr("d", `M${M.toString()}L${L.toString()}`)
            .attr('data-level', d.level)
            .attr("stroke", "url(#MyGradient)")
            .attr("stroke-width", 2)
      }
    });
  }

  /**
   * draws short data bars
   */
  drawShortData () {
    // short data array
    let shortData: Datum[] = this.data.levels.shortData;
    
    // append g container for path elements
    let rectGroup = this.chart.append("g").attr("class", "short-data");

    // join data
    let rects = rectGroup.selectAll('rect.short-data-rect')
                  .data(shortData)

    // apend new rects
    rects = rects.enter()
              .append('rect')
              .attr('class', 'short-data-rect')
              .attr('x', d => this.xScale(new Date(d.dateTime)))
              .attr('y', d => this.yScale(d.level.toUpperCase()) + this.yScale.bandwidth() / 2 - 6)
              .attr('width', d => this.getCurveWidth(d))
              .attr('height', this.curveInnerHeight + 3)
              .attr('rx', 2)
              .attr('ry', 2)
              .attr("fill", "url(#MyGradient)")
              .merge(rects)
    
    // remove old rects
    rects.exit().remove();
  }

  getCurveWidth (d) {
    // calculate curve width
    let currDt = new Date(d.dateTime);
    currDt.setSeconds(currDt.getSeconds() + d.seconds);
    const curveWidth = this.xScale(currDt) - this.xScale(new Date(d.dateTime));
    return curveWidth
  }

  /**
   * compares current data to previous and next sleep data and returns curve direction
   * @param current current sleep data
   * @param previous previous sleep data
   * @param next next sleep data
   */
  detectDirection(current, previous, next){
    let direction = '';

    // if previous is null, it is first sleep data
    if (!previous) {
      if (this.levels[current.level] < this.levels[next.level]) {
        direction = 'roundLUp';
      }
      else
      {
        direction = 'roundL';
      }
    }
    // if next is null, it is last sleep data
    else if (!next) {
      if (this.levels[previous.level] > this.levels[current.level]) {
        direction = 'roundR';
      }
      else {
        direction = 'roundRDown'
      }
    } 
    // if previous, next are not null, we are in the middle of the chart
    else if (this.levels[previous.level] > this.levels[current.level] &&
      this.levels[next.level] > this.levels[current.level]) {
      direction = 'down';
    }
    else if(this.levels[previous.level] < this.levels[current.level] &&
      this.levels[next.level] > this.levels[current.level])
    {
      direction = 'curlyR';
    }
    else if(this.levels[previous.level] > this.levels[current.level] &&
      this.levels[next.level] < this.levels[current.level]) {
      direction = 'curlyL';
    }
    else {
      direction = 'up';
    }
    
    return direction;
  }
}
