import { PieData, Summary } from '../data/models';

/**
 * format the date as dd MM YYYYY
 * @param date date object
 */
export function formatDate(date) {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
  
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
  
    return `${monthNames[monthIndex]} ${day}, ${year}`;
  }

/**
 * maps summery object to pie data object
 * @param summary summery object
 * @param total total number slept in minutes
 */
export function mapToPieData(summary: Summary, total: number): PieData[] {
  let arr = [];
  for(let key in summary) {
    let minutes = summary[key].minutes
    let r = Math.round((minutes / total) * 100)
    arr.push({
      value: r,
      minutes: minutes,
      name: key
    })
  }
  return arr;
}

/**
 * generates d attribute for svg path element, based of direction
 * @param startCoords object of x and y coordinates
 * @param curveWidth width of the path element
 * @param curveHeight height of the path element
 * @param curveInnerHeight thickness of the curve
 * @param direction shape of the curve
 */
export function getDAttr(startCoords, curveWidth, curveHeight, curveInnerHeight, direction)
{
  switch(direction)
  {
    case 'up':
      startCoords.y = startCoords.y + curveHeight - curveInnerHeight;
      return `M ${startCoords.x} ${startCoords.y - curveInnerHeight} 
            c 0 -${curveHeight - curveInnerHeight} 0 -${curveHeight - curveInnerHeight} ${curveWidth / 2} -${curveHeight - curveInnerHeight}
            c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} ${curveHeight - curveInnerHeight}
            l 0 ${curveInnerHeight}
            c 0 -${curveHeight - curveInnerHeight} 0 -${curveHeight - curveInnerHeight} -${curveWidth / 2} -${curveHeight - curveInnerHeight}
            c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} ${curveHeight - curveInnerHeight}
            Z`;
    case 'curlyL':
      startCoords.y = startCoords.y - curveHeight / 2 - curveInnerHeight / 2;
      return `M ${startCoords.x} ${startCoords.y} 
            c 0 ${curveHeight / 2 - curveInnerHeight / 2} 0 ${curveHeight / 2 - curveInnerHeight / 2} ${curveWidth / 2} ${curveHeight / 2 - curveInnerHeight / 2}
            c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} ${curveHeight / 2 - curveInnerHeight / 2}
            l 0 ${curveInnerHeight}
            c 0 -${(curveHeight / 2 - curveInnerHeight/2)} 0 -${(curveHeight / 2 - curveInnerHeight/2)} -${curveWidth / 2} -${(curveHeight / 2 - curveInnerHeight/2)}
            c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} -${(curveHeight / 2  - curveInnerHeight/2)}
            Z`
    case 'down':
    startCoords.y = startCoords.y - curveHeight;
      return `M ${startCoords.x} ${startCoords.y + curveInnerHeight} 
            c 0 ${curveHeight - curveInnerHeight} 0 ${curveHeight - curveInnerHeight} ${curveWidth / 2} ${curveHeight - curveInnerHeight}
            c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} -${(curveHeight - curveInnerHeight)}
            l 0 -${curveInnerHeight}
            c 0 ${(curveHeight - curveInnerHeight)} 0 ${(curveHeight - curveInnerHeight)} -${curveWidth / 2} ${(curveHeight - curveInnerHeight)}
            c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} -${(curveHeight - curveInnerHeight)}
            Z`;
    case 'curlyR':
      startCoords.y = startCoords.y + curveHeight / 2 - curveInnerHeight / 2;
      return `M ${startCoords.x} ${startCoords.y} 
            c 0 -${curveHeight / 2 - curveInnerHeight / 2} 0 -${curveHeight / 2 - curveInnerHeight / 2} ${curveWidth / 2} -${curveHeight / 2 - curveInnerHeight / 2}
            c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} -${curveHeight / 2 - curveInnerHeight / 2}
            l 0 -${curveInnerHeight}
            c 0 ${(curveHeight / 2 - curveInnerHeight/2)} 0 ${(curveHeight / 2 - curveInnerHeight/2)} -${curveWidth / 2} ${(curveHeight / 2 - curveInnerHeight/2)}
            c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} ${(curveHeight / 2  - curveInnerHeight/2)}
            Z`;
    case 'roundL':
      startCoords.y = startCoords.y - curveInnerHeight;
      return `M ${startCoords.x} ${startCoords.y + curveInnerHeight / 2} 
            c 0 -${curveInnerHeight / 2} 0 -${curveInnerHeight / 2} ${curveWidth / 2} -${curveInnerHeight / 2}
            c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} ${curveHeight - curveInnerHeight}
            l 0 ${curveInnerHeight}
            c 0 -${curveHeight - curveInnerHeight} 0 -${curveHeight - curveInnerHeight} -${curveWidth / 2} -${curveHeight - curveInnerHeight}
            c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} -${curveInnerHeight / 2}
            Z`;
    case 'roundR':
      startCoords.y = startCoords.y - curveHeight + curveInnerHeight * 0.75;
      return `M ${startCoords.x} ${startCoords.y} 
            c 0 ${curveHeight - curveInnerHeight} 0 ${curveHeight - curveInnerHeight} ${curveWidth / 2} ${curveHeight - curveInnerHeight}
            c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} -${curveInnerHeight / 2}
            c 0 -${curveInnerHeight / 2} 0 -${curveInnerHeight / 2} -${curveWidth / 2} -${curveInnerHeight / 2}
            c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} -${curveHeight - curveInnerHeight}
            Z`;
    case 'roundLUp':
      startCoords.y = startCoords.y - curveInnerHeight;
      return `M ${startCoords.x} ${startCoords.y + curveInnerHeight / 2} 
                c 0 -${curveInnerHeight / 2} 0 -${curveInnerHeight / 2} ${curveWidth / 2} -${curveInnerHeight / 2}
                c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} -${curveHeight - curveInnerHeight}
                l 0 ${curveInnerHeight}
                c 0 ${curveHeight - curveInnerHeight} 0 ${curveHeight - curveInnerHeight} -${curveWidth / 2} ${curveHeight - curveInnerHeight}
              c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} -${curveInnerHeight / 2}
              Z`;
    case 'roundRDown':
      startCoords.y = startCoords.y + curveHeight - curveInnerHeight;
      return `M ${startCoords.x} ${startCoords.y - curveInnerHeight} 
                c 0 -${curveHeight - curveInnerHeight} 0 -${curveHeight - curveInnerHeight} ${curveWidth / 2} -${curveHeight - curveInnerHeight}
                c ${curveWidth / 2} 0 ${curveWidth / 2} 0 ${curveWidth / 2} ${curveInnerHeight / 2}
                c 0 ${curveInnerHeight / 2} 0 ${curveInnerHeight / 2} -${curveWidth / 2} ${curveInnerHeight / 2}
                c -${curveWidth / 2} 0 -${curveWidth / 2} 0 -${curveWidth / 2} ${curveHeight - curveInnerHeight}
              Z`;
    default:
      return null;
  }
}