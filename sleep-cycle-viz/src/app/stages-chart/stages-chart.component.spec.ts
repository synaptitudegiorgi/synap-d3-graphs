import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StagesChartComponent } from './stages-chart.component';

describe('StagesChartComponent', () => {
  let component: StagesChartComponent;
  let fixture: ComponentFixture<StagesChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StagesChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagesChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
