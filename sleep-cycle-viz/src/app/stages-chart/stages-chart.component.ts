import { Component, OnInit } from '@angular/core';
import { Sleep, PieData, Summary } from '../data/models';
import { DataService } from '../data/data.service';
import { formatDate, mapToPieData } from '../helpers/index';

@Component({
  selector: 'app-stages-chart',
  templateUrl: './stages-chart.component.html',
  styleUrls: ['./stages-chart.component.css']
})
export class StagesChartComponent implements OnInit {
  ngOnInit() {}
  colors = 
  {
    deep: 'rgb(37.756348%,19.833374%,76.774597%)',
    wake: 'rgb(87.487793%,2.069092%,42.9245%)',
    light: 'rgb(72.038269%,10.650635%,54.515076%)',
    rem: 'rgb(47.462463%,24.090576%,61.839294%)'
  }
  sleep: Sleep;
  date: string;
  pieData: PieData[];
  isDataAvailable: boolean = false;
  isClassicDataAvailable: boolean = false;

  constructor(private dataService: DataService) {
    // dataService.getData().subscribe(d => {
    //   this.sleep = d.sleep[0];
    //   this.date = formatDate(new Date(this.sleep.dateOfSleep));
    //   this.pieData = mapToPieData(this.sleep.levels.summary, this.sleep.timeInBed);
    //   this.isDataAvailable = true;
    // });

    dataService.getMultipleNights().subscribe(d => {
      this.sleep = d.sleep[8];
      this.date = formatDate(new Date(this.sleep.dateOfSleep));
      this.pieData = mapToPieData(this.sleep.levels.summary, this.sleep.timeInBed);
      this.isDataAvailable = true;
    });
  }
}
