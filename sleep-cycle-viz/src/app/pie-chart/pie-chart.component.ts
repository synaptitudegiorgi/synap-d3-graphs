import { Component, OnInit, Input, AfterViewChecked } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit, AfterViewChecked {
  @Input('data') data;
  @Input('color') color;
  id: string = '';
  width = 100;
  height = 130;
  thickness = 10;
  backColor = 'rgb(52.49939%,52.49939%,52.49939%)';
  margin = {
    top: 5,
    right: 5,
    bottom: 5,
    left: 5
  };

  constructor() {
  }

  ngAfterContentChecked() {
    if (this.data) {
      this.id = `pie-${this.data.name}`;
    }
  }

  ngAfterViewInit() {
    
  }

  ngAfterViewChecked() {
    if (this.data) {
      this.main();
    }
  }

  ngOnInit() {
  }

  /**
   * main drawing function
   */
  main() {
    let svg = d3.select(`#${this.id}`);

    // pie data structure
    let data: any = [
      {
        value: this.data.value
      },
      {
        value: 100 - this.data.value
      }
    ];

    // setupDimensions
    const width = this.width - this.margin.left - this.margin.right
    const height = this.height - this.margin.top - this.margin.bottom
    const radius = Math.min(width, height) / 2 - 15;
    
    // create the arc function
    const arc = d3.arc()
    .innerRadius(radius - this.thickness)
    .outerRadius(radius)

    // creat the pie function
    const pie = d3.pie()
        .value(function(d: any): number {
          return d.value;
        })
        .sort(null)

    // initialize the donut holder
    const container = svg.append('g')
      .attr('class', 'donut')
      .attr('transform', 'translate(' + (width / 2 + this.margin.left) + ',' + (width / 2 + this.margin.top) + ')')
    
    // slices enter selection
    let slices = container.selectAll('slice')
                          .data(pie(data))
    
    // append new path elements
    slices = slices.enter()
                   .append('path')
                   .attr('class', 'slice')
                   .merge(slices);

    // set d attribute
    slices.attr('d', function(d: any): string{
      let tmp = arc(d);
      return tmp;
    })
    .attr('fill', (d,i) => {
      if (i > 0) {
        return this.backColor;
      }
      return this.color;
    })

    // remove old path elements
    slices.exit().remove();

    let text = container.selectAll('text.percent')
                  .data([this.data.value])
    
    // remove selection
    text.exit().remove()

    // center texts
    text = text.enter()
              .append('text')
              .text(d => d + '%')
              .attr('font-size', 14)
              .attr('class', 'percent')
              .attr('fill', this.backColor)
              .attr('text-anchor','middle')
              .attr('alignment-baseline','middle')
              .attr('font-family','Helvetica')
              .merge(text)
    
    let name = container.selectAll('text.name')
                .data([this.data.name])

    name = name.enter()
       .append('text')
       .text(d => d)
       .attr('font-size', 18)
       .attr('class', 'name')
       .attr('dy', radius + 14)
       .attr('fill', this.backColor)
       .attr('text-anchor','middle')
       .attr('alignment-baseline','middle')
       .attr('font-family','Helvetica')
       .merge(name)
    
    let hours = container.selectAll('text.hours')
       .data([this.formatMinutes(this.data.minutes)])

    hours = hours.enter()
       .append('text')
       .text(d => d)
       .attr('font-size', 14)
       .attr('dy', radius + 30)
       .attr('fill', this.backColor)
       .attr('text-anchor','middle')
       .attr('alignment-baseline','middle')
       .attr('font-family','Helvetica')
       .merge(hours)
  }

  /**
   * format minutes into HH MM
   * @param minutes minutes to format
   */
  formatMinutes(minutes: number): string {
    let h = Math.floor(minutes / 60);
    let m = minutes % 60;
    let str = '';
    if (h > 0) {
      str += `${h}h `;
    }
    if (m > 0) {
      str += `${m}m`;
    }
    return str;
  }
}
