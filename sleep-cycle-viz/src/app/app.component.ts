import { Component } from '@angular/core';
import { ChartData, Sleep, PieData, Summary } from './data/models';
import { DataService } from './data/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
