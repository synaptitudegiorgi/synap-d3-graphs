import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ChartData } from './models';

@Injectable()
export class DataService {

  constructor(private http: HttpClient) { }

  getData(): Observable<ChartData> {
    return this.http
               .get<ChartData>('../../assets/stages/63SB6V_2018-06-01.json');
  }

  getClassicData (): Observable<ChartData> {
    return this.http
               .get<ChartData>('../../assets/classic/6HYV4Q_2018-04-23.json')
  }

  getMultipleNights (): Observable<ChartData> {
    return this.http
              .get<ChartData>('../../assets/stages/6DJJTW.json')
  }
}
