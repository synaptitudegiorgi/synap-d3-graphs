export interface ChartData {
  sleep: Sleep[];
  summary: Summary2;
}

export interface Summary2 {
  stages: Stages;
  totalMinutesAsleep: number;
  totalSleepRecords: number;
  totalTimeInBed: number;
}

export interface Stages {
  deep: number;
  light: number;
  rem: number;
  wake: number;
}

export interface Sleep {
  dateOfSleep: string;
  duration: number;
  efficiency: number;
  endTime: string;
  infoCode: number;
  isMainSleep: boolean;
  levels: Levels;
  logId: number;
  minutesAfterWakeup: number;
  minutesAsleep: number;
  minutesAwake: number;
  minutesToFallAsleep: number;
  startTime: string;
  timeInBed: number;
  type: string;
}

export interface Levels {
  data: Datum[];
  shortData: Datum[];
  summary: Summary;
}

export interface Summary {
  deep: Deep;
  light: Deep;
  rem: Deep;
  wake: Deep;
}

export interface Deep {
  count: number;
  minutes: number;
  thirtyDayAvgMinutes: number;
}

export interface Datum {
  dateTime: string;
  level: string;
  seconds: number;
}

export interface PieData {
  value: number,
  minutes: number,
  name: string
}
