import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { D3VizClassicComponent } from './d3-viz-classic.component';

describe('D3VizClassicComponent', () => {
  let component: D3VizClassicComponent;
  let fixture: ComponentFixture<D3VizClassicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ D3VizClassicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(D3VizClassicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
