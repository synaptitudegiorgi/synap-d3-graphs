import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import * as d3 from 'd3';
import { AxisDomain } from 'd3';
import { Levels, Datum } from '../data/models';

@Component({
  selector: 'app-d3-viz-classic',
  templateUrl: './d3-viz-classic.component.html',
  styleUrls: ['./d3-viz-classic.component.css']
})
export class D3VizClassicComponent implements AfterViewInit, OnInit {
  @ViewChild('sleepvizclassic') container;
  @Input('data') data;
  xScale: d3.AxisScale<AxisDomain>;
  yScale: d3.AxisScale<AxisDomain>;
  width: number;
  height = 450;
  margin = {
    top: 40,
    right: 15,
    bottom: 50,
    left: 15
  };
  colors = {
    awake: '#e50269ff',
    asleep: '#5d31c9ff',
    restless: '#8a76ffff'
  }
  // height difference between asleep and other levels
  asleepHeightDiff = 60;
  chart: d3.Selection<d3.BaseType, {}, HTMLElement, any>;
  constructor() { }

  ngOnInit() {
  }

  ngAfterContentChecked() {
    if (this.data) {
      this.setScales();
      this.drawAxis();
      this.main();
    }
  }

  ngAfterViewInit() {
    const bclient = this.container.nativeElement.getBoundingClientRect();
    this.width = bclient.width - 10;
  }

  /**
   * sets x time scale
   */
  setScales() {
    const xDomain = [this.data.levels.data[0].dateTime, this.data.endTime];
    this.xScale = d3.scaleTime()
                  .domain([new Date(xDomain[0]), new Date(xDomain[1])])
                  .range([0, this.width - this.margin.left - this.margin.right]);
  }

  /**
   * creates and returns xAxis generator based on x scale
   */
  getAxis() {
    const xAxis = d3.axisBottom(this.xScale);
    return {
      xAxis: xAxis
    };
  }

  /**
   * draws x axis
   */
  drawAxis() {
    const axes = this.getAxis();
    this.chart = d3.select("#svg-container-classic")
                  .append("g")
                  .attr("transform", `translate(${this.margin.left}, ${this.margin.top})`);

    this.chart.append("g")
        .attr("class", "xAxis")
         .attr("transform", `translate(0, ${this.height - this.margin.top - this.margin.bottom})`)
         .call(axes.xAxis);
  }

  /**
   * main drawing function
   */
  main() {
    let data: Datum[] = this.data.levels.data;
    let chartHeight = this.height - this.margin.top - this.margin.bottom;

    // join data
    let bars = this.chart.selectAll('rect.bar')
                        .data(data);

    // bars enter selection
    bars = bars.enter()
               .append('rect')
               .attr('width', d => {
                 return this.xScale(new Date(d.dateTime).setSeconds(d.seconds)) - this.xScale(new Date(d.dateTime));
               })
               .attr('height', d => {
                 if (d.level == 'asleep') {
                  return chartHeight - this.asleepHeightDiff
                 }
                 return chartHeight
               })
               .attr('x', d => this.xScale(new Date(d.dateTime)))
               .attr('y', d => {
                if (d.level == 'asleep') {
                  return this.asleepHeightDiff
                 }
                 return 0
               })
               .attr('fill', d => this.colors[d.level])
  }

}
