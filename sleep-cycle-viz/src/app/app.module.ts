import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { D3VizComponent } from './d3-viz/d3-viz.component';

import { HttpClientModule } from '@angular/common/http';
import { DataService } from './data/data.service';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { D3VizClassicComponent } from './d3-viz-classic/d3-viz-classic.component';
import { ClassicChartComponent } from './classic-chart/classic-chart.component';
import { StagesChartComponent } from './stages-chart/stages-chart.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/stages', pathMatch: 'full'  },
  { path: 'stages', component: StagesChartComponent },
  { path: 'classic',      component: ClassicChartComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    D3VizComponent,
    PieChartComponent,
    D3VizClassicComponent,
    ClassicChartComponent,
    StagesChartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
