import { Component, OnInit } from '@angular/core';
import { Sleep, PieData } from '../data/models';
import { DataService } from '../data/data.service';
import { formatDate, mapToPieData } from '../helpers/index';

@Component({
  selector: 'app-classic-chart',
  templateUrl: './classic-chart.component.html',
  styleUrls: ['./classic-chart.component.css']
})
export class ClassicChartComponent implements OnInit {
  ngOnInit() {
  }
  colors = {
    awake: '#e50269ff',
    asleep: '#5d31c9ff',
    restless: '#8a76ffff'
  }
  sleep: Sleep;
  date: string;
  pieData: PieData[];
  isDataAvailable: boolean = false;

  constructor(private dataService: DataService) {
    dataService.getClassicData().subscribe(d => {
      this.sleep = d.sleep[0];
      this.date = formatDate(new Date(this.sleep.dateOfSleep));
      this.pieData = mapToPieData(this.sleep.levels.summary, this.sleep.timeInBed);
      this.isDataAvailable = true;
    });
  }
}
