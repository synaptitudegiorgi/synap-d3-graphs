import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassicChartComponent } from './classic-chart.component';

describe('ClassicChartComponent', () => {
  let component: ClassicChartComponent;
  let fixture: ComponentFixture<ClassicChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassicChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
